import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("products",(t)=>{
        t.increments("id").unsigned().primary()
        t.string("name").notNullable()
        t.integer("price").notNullable()
        t.boolean("expire").defaultTo(false)
        t.dateTime("expire_date")
        t.timestamp("created_at").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP()"))
        t.timestamp("updated_at").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP()"))
    })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("products")
}

