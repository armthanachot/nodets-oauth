import db from "../../../config/db_connection"
const create = async(user)=>{
    const result = db.query(`INSERT INTO users SET ?`,[user])
    return result
}

const multi_create = async(users)=>{
    const result = db.query(`INSERT INTO users(name,lname) VALUES ?`,[users])
    return result
}

const showTbl = async()=>{
    const result = await db.query("SHOW TABLES")
    return result
}

export {
    create,
    multi_create,
    showTbl
}