import db from "../../../config/db_connection";

const findAll = async()=>{
  const result = await db.query("SELECT * FROM products")
  return result
}

const create = async (product) => {
  const result = await db.query("INSERT INTO products SET ?", [product]);
  return result;
};

export { findAll,create };
