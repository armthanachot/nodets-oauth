import * as userModel from "../../models/user/user.model";
const findAll = async (req, res) => {
  try {
    const users = [
      {
        name: "xxxx",
      },
      {
        name: "test",
      },
    ];
    return res.status(200).json({ data: users });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });
  }
};

const findById = async (req, res) => {
  try {
    const user = {
      name: "1st user",
    };
    return res.status(200).json({ data: user, id: req.params.id });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });
  }
};

const create = async (req, res) => {
  try {
    const user = req.body;
    let created = null
    if (Array.isArray(user)) {
      const users = [];
      user.map((item) => {
        return users.push(Object.values(item));
      });

      created = await userModel.multi_create(users);
      if (!created.affectedRows) {
        return res.status(400).json({ message: "CANNOT CREATE USER" });
      }
      return res.status(200).json({ message: "OK" });
    }
    created = await userModel.create(user);
    if (!created.affectedRows) {
      return res.status(400).json({ message: "CANNOT CREATE USER" });
    }
    return res.status(200).json({ message: "OK" });
  } catch (error) {
    console.log(error.message);

    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });
  }
};

const showTbl = async(req,res)=>{
  try {
    const tables = await userModel.showTbl()
    console.log("tables: ",tables);

    return res.status(200).json({data:tables})
  } catch (error) {
    console.log(error.message);

    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });

  }
}
export { create, findAll, findById,showTbl };
