import * as productModel from "../../models/product/product.model";

const findAll = async (req, res) => {
  try {
    const products = await productModel.findAll();
    return res.status(200).json({ data: products });
  } catch (error) {
    console.log(error.message);

    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });
  }
};

const create = async (req, res) => {
  try {
    const product = req.body;
    const created = await productModel.create(product);
    if (!created.affectedRows) {
      return res.status(400).json({ message: "CANNOT CREATE" });
    }
    return res.status(200).json({ message: "OK" });
  } catch (error) {
    console.log(error.message);

    return res.status(500).json({ message: "INTERNAL SERVER ERROR" });
  }
};

export { create, findAll };
