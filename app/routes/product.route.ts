import {findAll,create} from "../controllers/product/product.controller"

export default (prefix,app)=>{
    app.route(`${prefix}`).get([],findAll).post([],create)
}