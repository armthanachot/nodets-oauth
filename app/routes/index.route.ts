/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

"use strict"

/**
 * Load app configurations
 */

import userRoute from "./user.route"
import authRoute from "./auth.route"
import productRoute from "./product.route"

import { APIURL } from "../constants/config"
export default (app) => {
  userRoute(`${APIURL}/user`, app)
  authRoute(`${APIURL}/auth`, app)
  productRoute(`${APIURL}/product`, app)
}
