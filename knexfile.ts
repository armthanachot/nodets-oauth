import {MYSQL} from "./config/env/development"
module.exports = {
    client: "mysql",
    connection: MYSQL,
    migrations:{
      directory:__dirname + '/db/migrations'
    },
    seeds:{
      directory: __dirname + '/db/seeds'
    }
}
