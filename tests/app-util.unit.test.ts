import {findOne,add} from "../app/utils/app"

describe("test find one",()=>{
    const mock = [
        {name:"Thanachot"},{name:"Phattranit"}
    ]
    test("find one",async (done)=>{
        const result = await findOne(mock)
        expect(result).toMatchObject({name:"Thanachot"})
        done()
    })

})

describe("plus number",()=>{
    test("a+b",async(done)=>{
        const result = await add(1,2)
        expect(result).toEqual(3)
        done()
    })
})