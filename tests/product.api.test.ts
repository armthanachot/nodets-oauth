import * as supertest from "supertest";
import app from "../config/express";

test("GET /api/v1/product", async (done) => {
  const tested = await supertest(app).get("/api/v1/product")
    .set("Accept", "application/json")
    .expect(200);
  done();
});

test("POST /api/v1/product", async (done) => {
  const product = {
    name: "lamp",
    price: 1500,
  };
  const tested = await supertest(app).post("/api/v1/product")
    .set("Accept", "application/json")
    .send(product)
    .expect(200);
  const { message } = tested.body;
  expect(message).toEqual("OK");
  done();
});
