import * as supertest from "supertest";
import app from "../config/express";

test("GET /api/v1/user", async (done) => {
  const tested = await supertest(app).get("/api/v1/user")
    .set("Accept", "application/json")
    .expect(200);
  const { data } = tested.body;
  expect(data).toEqual([{ name: "xxxx" }, { name: "test" }]);
  done()

});


test("GET /api/v1/user/1", async (done) => {
  const tested = await supertest(app).get("/api/v1/user/1")
    .set("Accept", "application/json")
    .expect(200);
  const { data, id } = tested.body;
  expect(data).toEqual({ name: "1st user" });
  expect(id).toEqual("1");
  done()

});

test("GET /api/v1/user/showtbl", async (done) => {
  const tested = await supertest(app).get("/api/v1/user/showtbl")
    .set("Accept", "application/json")
    .expect(200);
    done()

});

test("POST /api/v1/user", async (done) => {
  const data = [{
    name: "Thanachot",
    lname: "Tesjaroen",
  },
  {
    name:"Patchara",
    lname:"Mangkorn"
  }
];
  const tested = await supertest(app).post("/api/v1/user").set("Accept","application/json").send(data).expect(200);
  const {message} = tested.body
  expect(message).toEqual("OK")
  done()
});
